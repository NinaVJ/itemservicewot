package com.wotwiki.itemservice.model;

public class Item {

    private String itemName;
    private boolean magical;
    private String itemId;

    public Item(String name, boolean magical, String itemId) {
        this.itemName = name;
        this.magical = magical;
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public boolean isMagical() {
        return magical;
    }

    public void setMagical(boolean magical) {
        this.magical = magical;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
}
