package com.wotwiki.itemservice.controller;

import com.wotwiki.itemservice.model.Item;
import com.wotwiki.itemservice.repository.ItemRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//make it a rest resource.controller
@RestController//now it is a Rest controller. Spring boot is going to ask: do you have a mapping for this?
@RequestMapping("/item")//als iemand item callt, wordt deze klasse geladen
public class ItemController {

//LET OP: METHODE BUITEN KLASSE ZETTEN
    @GetMapping("/{itemId}") //{} tells that it is a variable, not literallt itemId
    public Item getItemById(@PathVariable("itemId") String itemId){
        List<Item> itemList = ItemRepository.getItemList();//ik doe alsof ik de lijst ophaal en haal uit de list de juiste item en return die
        for (int i = 0; i< itemList.size(); i++){//checken of iD gelijk is aan de Id in parameter
            if(itemList.get(i).getItemId().equals(itemId)){
                return itemList.get(i);//return Item op de positie van ItemId
            }
        }
        return null;
    }

}
