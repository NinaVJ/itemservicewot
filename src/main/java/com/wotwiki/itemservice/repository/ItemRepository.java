package com.wotwiki.itemservice.repository;

import com.wotwiki.itemservice.model.Item;

import java.util.ArrayList;
import java.util.List;

public class ItemRepository {

    public static List<Item> getItemList() {

        ArrayList<Item> itemlist = new ArrayList<Item>();
        itemlist.add(new Item("Callandor", true, "callandor"));
        itemlist.add(new Item("Mah'alleinir", true, "hammer"));
        itemlist.add(new Item("Silver foxhead medallion", true, "medallion"));
        itemlist.add(new Item("Ashandarei", true, "swordspear"));
        itemlist.add(new Item("Hadori", false, "headband"));
        itemlist.add(new Item("Heron marked blade", false, "heronsword"));
        itemlist.add(new Item("Amrylin Stole", false, "stole"));
        itemlist.add(new Item("Great Serpent ring", false, "ringofaessedai"));
        return itemlist;
    }
}
